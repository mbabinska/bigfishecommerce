<?php

namespace ApiBundle\Tests\Controller;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class CartControllerTest
 *
 * @package ApiBundle\Tests\Controller
 */
class CartControllerTest extends WebTestCase
{
    public function testCartConfigurations()
    {
        $client = static::createClient();

        /** @var EntityManager $manager */
        $manager = static::$kernel->getContainer()->get('doctrine')->getManager();

        # Should use TEST DB in real app #
        $this->cleanDB($manager);

        # We imagine the user to be logged in already and that a JWT token is sent alongside the request #
        $client->request('GET', '/api/cart/10');
        $response = $client->getResponse();
        $json = json_decode($response->getContent(), true);

        # We expect an empty cart since we truncated it #
        $this->assertEquals(null, $json);

        # We add a product and test for cart data #
        $client->request('POST', '/api/cart/add/10/32');
        $response = $client->getResponse();
        $json = json_decode($response->getContent(), true);
        $this->assertEquals(1, count($json['products']));

        # We add a product with discount and test for cart prices and discount #
        $client->request('POST', '/api/cart/add/10/32');
        $client->request('POST', '/api/cart/add/10/32');
        $response = $client->getResponse();
        $json = json_decode($response->getContent(), true);

        $this->assertEquals(3, $json['numberOfproducts']);
        $this->assertEquals(3900 * 2, $json['sumDiscountedPrice']);
        $this->assertEquals(3900, $json['discount']);

        # We remove a product and test cart data #
        $client->request('POST', '/api/cart/remove/10/32');
        $response = $client->getResponse();
        $json = json_decode($response->getContent(), true);
        $this->assertEquals(2, $json['numberOfproducts']);
        $this->assertEquals(3900 * 2, $json['sumDiscountedPrice']);
        $this->assertEquals(0, $json['discount']);

        # We remove a product which is not in the cart and test for error #
        $client->request('POST', '/api/cart/remove/10/3332');
        $response = $client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());
    }

    private function cleanDB(EntityManager $manager)
    {
        $stmt = $manager->getConnection()->prepare('DELETE FROM cart_has_product');
        $stmt->execute();
        $stmt = $manager->getConnection()->prepare('DELETE FROM cart');
        $stmt->execute();
    }
}
