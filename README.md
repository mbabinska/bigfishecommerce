Installation instructions
 
1.    We need to clone the source code from our SVN into our web folder: “git clone https://bitbucket.org/mbabinska/bigfishecommerce.git”
2.     Open the project in a Terminal or cmd(windows)
3.     Run composer update in project root – you will be asked to enter some default information, fill in the database host, port, user, password and leave the rest as is by default
4.     Import the database dump which is located in the dumpdb folder in the project root
        •	I used Doctrine ORM , so you could as well create the database via default symfony console commands:
            o	php bin/console doctrine:database:create  (the name of db bis: bigfishecommerce), 
            o	php bin/console doctrine:schema:update --force 
        •	I prepared dummy data via data fixtures, so you can load them via:
            o	php bin/console doctrine:fixtures:load
        •	Tests can be launched via the following command:
            o	Phpunit (I wrote test using data from dump-ed DB. If you create your own database, these tests needs to change ID-s of products)
5.     Run the application via the following command php bin/console server:run
6.     Launch the application at localhost:8000
7.     In case of errors please check permissions for cache folders /var/cache or check http://localhost:8000/config.php if you meet the systems basic requirements
 
Requirements: Basic Symfony requirements, PHP 7, Mysql 5+ db, configured Apache2 web server (htaccess and mod rewrite should be allowed)
