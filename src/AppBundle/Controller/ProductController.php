<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class ProductController
 *
 * @package AppBundle\Controller
 * @Route("/product")
 */
class ProductController extends Controller
{
    /**
     * List of products
     *
     * @Route("/", name="product_list")
     *
     * @return array
     */
    public function listAction()
    {
        return $this->render('AppBundle:Product:list.html.twig', [
    ]);
    }
}
