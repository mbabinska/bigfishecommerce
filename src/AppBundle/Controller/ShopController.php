<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class ShopController
 *
 * @package AppBundle\Controller
 *
 * @Route("/shop")
 */
class ShopController extends Controller
{
    /**
     * @Route("/", name="shop_index")
     *
     * @return array
     */
    public function indexAction()
    {
        return $this->render('AppBundle:Shop:index.html.twig', [
        ]);
    }
}
