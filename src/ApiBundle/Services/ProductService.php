<?php

namespace ApiBundle\Services;

use ApiBundle\Entity\Author;
use ApiBundle\Entity\Discount;
use ApiBundle\Entity\DiscountType;
use ApiBundle\Entity\Product;
use ApiBundle\Entity\Publisher;

/**
 * Class ProductService
 */
class ProductService
{
    /**
     * Process array of product objects
     *
     * @param array $products
     *
     * @return array
     */
    public function processProducts($products)
    {
        $productsArray = [];

        /** @var Product $product */
        foreach ($products as $product) {
            $productArray = $this->processProduct($product);
            $productsArray[] = $productArray;
        }

        return $productsArray;
    }

    /**
     * @param Product $product
     *
     * @return array
     */
    public function processProduct($product, $amount = false)
    {
        /** @var Publisher $publisher */
        $publisher = $product->getPublisher();

        $authors = $product->getAuthors();
        $authorsArray = [];
        /** @var Author $author */
        foreach ($authors as $author) {
            $authorsArray[] = $author->getName();
        }

        $authors = implode(',', $authorsArray);

        $discountedPrice = $this->countDiscountPrice($product->getPrice(), $product->getDiscounts(), $publisher->getDiscount());

        if ($discountedPrice === $product->getPrice()) {
            $isDiscounted = false;
        } else {
            $isDiscounted = true;
        }

        $productArray = [
            'id'                         => $product->getId(),
            'title'                      => $product->getTitle(),
            'authors'                    => $authors,
            'publisher'                  => $publisher->getName(),
            'price'                      => $product->getPrice(),
            'discountedPrice'            => $discountedPrice,
            'isDiscounted'               => $isDiscounted,
            'amount'                     => $amount,
            'amountAfterPackageDiscount' => $amount,
        ];

        return $productArray;
    }

    /**
     * Return discounted price for product
     *
     * @param float $productPrice
     * @param       $productDiscounts
     * @param       $publisherDiscounts
     *
     * @return false|float
     */
    private function countDiscountPrice($productPrice, $productDiscounts, $publisherDiscounts)
    {
        $newPrice = $productPrice;

        if ($publisherDiscounts) {
            /** @var Discount $discount */
            foreach ($publisherDiscounts as $discount) {
                $newPrice = $this->processAmountDiscount($discount, $newPrice);
            }
        }

        if ($productDiscounts) {
            /** @var Discount $discount */
            foreach ($productDiscounts as $discount) {
                $newPrice = $this->processAmountDiscount($discount, $newPrice);
            }
        }

        return $newPrice;
    }

    /**
     * Process discount
     *
     * @param Discount $discount
     * @param float    $price
     *
     * @return int|float
     */
    private function processAmountDiscount($discount, $price)
    {
        /** @var DiscountType $discountType */
        $discountType = $discount->getType()->getName();
        if ($discountType === 'amount') {
            $amount = $discount->getAmount();
            /** Discount is in percentage */
            if ($discount->getPercentage()) {
                $price *= 1 - ($amount / 100);
            } /** Discount is in sum */
            else {
                $price -= $amount;
            }
        }

        return $price;
    }
}