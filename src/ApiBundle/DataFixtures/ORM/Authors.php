<?php

namespace ApiBundle\DataFixtures;

use ApiBundle\Entity\Author;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Authors
 *
 * @package ApiBundle\DataFixtures
 */
class Authors implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Sets the Container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     *
     * @api
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $author1 = new Author();
        $author1->setName('Janine Warner');

        $author2 = new Author();
        $author2->setName('Sikos László');

        $author3 = new Author();
        $author3->setName('Barry Burd');

        $author4 = new Author();
        $author4->setName('Stephen Randy Davis');

        $author5 = new Author();
        $author5->setName('Joshua Eichorn');

        $author6 = new Author();
        $author6->setName('Ivanyos Gábor');

        $author7 = new Author();
        $author7->setName('Rónyai Lajos');

        $author8 = new Author();
        $author8->setName('Szabó Réka');

        $manager->persist($author1);
        $manager->persist($author2);
        $manager->persist($author3);
        $manager->persist($author4);
        $manager->persist($author5);
        $manager->persist($author6);
        $manager->persist($author7);
        $manager->persist($author8);

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 10;
    }
}