<?php

namespace ApiBundle\DataFixtures;

use ApiBundle\Entity\Author;
use ApiBundle\Entity\Product;
use ApiBundle\Entity\Publisher;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Products
 *
 * @package ApiBundle\DataFixtures
 */
class Products implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Sets the Container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     *
     * @api
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $product1 = new Product();
        $product1->setTitle('Dreamweaver CS4');
        $product1->setPrice(3900);
        $product1->addAuthor($this->getAuthor($manager, 'Janine Warner'));
        $product1->setPublisher($this->getPublisher($manager, 'PANEM'));

        $product2 = new Product();
        $product2->setTitle('JavaScript on client side');
        $product2->setPrice(2900);
        $product2->addAuthor($this->getAuthor($manager, 'Sikos László'));
        $product2->setPublisher($this->getPublisher($manager, 'BBS-INFO'));

        $product3 = new Product();
        $product3->setTitle('Java');
        $product3->setPrice(3700);
        $product3->addAuthor($this->getAuthor($manager, 'Barry Burd'));
        $product3->setPublisher($this->getPublisher($manager, 'PANEM'));

        $product4 = new Product();
        $product4->setTitle('C# 2008');
        $product4->setPrice(3700);
        $product4->addAuthor($this->getAuthor($manager, 'Stephen Randy Davis'));
        $product4->setPublisher($this->getPublisher($manager, 'PANEM'));

        $product5 = new Product();
        $product5->setTitle('Ajax Fundamentals');
        $product5->setPrice(4500);
        $product5->addAuthor($this->getAuthor($manager, 'Joshua Eichorn'));
        $product5->setPublisher($this->getPublisher($manager, 'PANEM'));

        $product6 = new Product();
        $product6->setTitle('Algorithms');
        $product6->setPrice(3600);
        $product6->addAuthor($this->getAuthor($manager, 'Ivanyos Gábor'));
        $product6->addAuthor($this->getAuthor($manager, 'Rónyai Lajos'));
        $product6->addAuthor($this->getAuthor($manager, 'Szabó Réka'));
        $product6->setPublisher($this->getPublisher($manager, 'TYPOTEX'));

        $manager->persist($product1);
        $manager->persist($product2);
        $manager->persist($product3);
        $manager->persist($product4);
        $manager->persist($product5);
        $manager->persist($product6);

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 20;
    }

    /**
     * @param ObjectManager $manager
     * @param string        $name
     *
     * @return Author
     */
    private function getAuthor(ObjectManager $manager, $name)
    {
        $author = $manager->getRepository('ApiBundle:Author')->findOneBy([
            'name' => $name,
        ]);

        return $author;
    }

    /**
     * @param ObjectManager $manager
     * @param string        $name
     *
     * @return Publisher
     */
    private function getPublisher(ObjectManager $manager, $name)
    {
        $publisher = $manager->getRepository('ApiBundle:Publisher')->findOneBy([
            'name' => $name,
        ]);

        return $publisher;
    }
}