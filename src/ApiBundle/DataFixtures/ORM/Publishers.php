<?php

namespace ApiBundle\DataFixtures;

use ApiBundle\Entity\Publisher;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Publishers
 *
 * @package ApiBundle\DataFixtures
 */
class Publishers implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Sets the Container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     *
     * @api
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $publisher1 = new Publisher();
        $publisher1->setName('PANEM');

        $publisher2 = new Publisher();
        $publisher2->setName('BBS-INFO');

        $publisher3 = new Publisher();
        $publisher3->setName('TYPOTEX');

        $manager->persist($publisher1);
        $manager->persist($publisher2);
        $manager->persist($publisher3);

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 11;
    }
}