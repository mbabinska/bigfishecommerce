<?php

namespace ApiBundle\DataFixtures;

use ApiBundle\Entity\Discount;
use ApiBundle\Entity\DiscountType;
use ApiBundle\Entity\Product;
use ApiBundle\Entity\Publisher;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Discounts
 *
 * @package ApiBundle\DataFixtures
 */
class Discounts implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Sets the Container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     *
     * @api
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $discount1 = new Discount();
        $discount1->setType($this->getDiscountType($manager, 'amount'));
        $discount1->setName('discount percent of 10%, calculated from item price');
        $discount1->setAmount(10);
        $discount1->setPercentage(true);
        $discount1->addProduct($this->getProduct($manager, 'Algorithms'));

        $discount2 = new Discount();
        $discount2->setType($this->getDiscountType($manager, 'amount'));
        $discount2->setName('amount discount of 500 HUF, calculated from item price');
        $discount2->setAmount(500);
        $discount2->setPercentage(false);
        $discount2->addProduct($this->getProduct($manager, 'JavaScript on client side'));

        $discount3 = new Discount();
        $discount3->setType($this->getDiscountType($manager, 'package'));
        $discount3->setName('2+1 package discount (the cheapest product in a set is free)');
        $discount3->setPayPackage(2);
        $discount3->setFreePackage(1);
        $discount3->addPublisher($this->getPublisher($manager, 'PANEM'));

        $manager->persist($discount1);
        $manager->persist($discount2);
        $manager->persist($discount3);

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 30;
    }

    /**
     * @param ObjectManager $manager
     * @param string        $name
     *
     * @return DiscountType
     */
    private function getDiscountType(ObjectManager $manager, $name)
    {
        $type = $manager->getRepository('ApiBundle:DiscountType')->findOneBy([
            'name' => $name,
        ]);

        return $type;
    }

    /**
     * @param ObjectManager $manager
     * @param string        $name
     *
     * @return Product
     */
    private function getProduct(ObjectManager $manager, $name)
    {
        $product = $manager->getRepository('ApiBundle:Product')->findOneBy([
            'title' => $name,
        ]);

        return $product;
    }

    /**
     * @param ObjectManager $manager
     * @param string        $name
     *
     * @return Publisher
     */
    private function getPublisher(ObjectManager $manager, $name)
    {
        $publisher = $manager->getRepository('ApiBundle:Publisher')->findOneBy([
            'name' => $name,
        ]);

        return $publisher;
    }
}