<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\Discount;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class DiscountController
 *
 * @package ApiBundle\Controller
 * @Route("/api/discounts")
 */
class DiscountController extends Controller
{
    /**
     * Return all discounts
     *
     * @Route("/", name="all_discounts")
     *
     * @return JsonResponse
     */
    public function getDiscountsAction()
    {
        $discounts = $this->getDoctrine()->getRepository('ApiBundle:Discount')->findAll();
        $discountsArray = [];

        if ($discounts) {
            /** @var Discount $discount */
            foreach ($discounts as $discount) {
                $discountsArray[$discount->getId()] = [
                    'id'   => $discount->getId(),
                    'name' => $discount->getName(),
                ];
            }

            return $this->json($discountsArray);
        } else {
            $this->json(['success' => false], 400);
        }
    }
}
