<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ProductController
 *
 * @package ApiBundle\Controller
 * @Route("/api/products")
 */
class ProductController extends Controller
{
    /**
     * Return all products with discounted prices
     *
     * @Route("/", name="all_products")
     *
     * @return JsonResponse
     */
    public function getAllProductsAction()
    {
        $products = $this->getDoctrine()->getRepository('ApiBundle:Product')->findAll();

        if ($products) {
            $productsArray = $this->get('product_service')->processProducts($products);

            return $this->json($productsArray);
        } else {
            return $this->json(['success' => false], 400);
        }

    }

    /**
     * Return all products with discounted prices, sorted by title
     *
     * @Route("/title", name="all_products_ordered_title")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getAllProductsSortedByTitleAction(Request $request)
    {
        $way = $request->get('order');
        if (!$way) {
            $way = 'asc';
        }

        $products = $this->getDoctrine()->getRepository('ApiBundle:Product')->findBy([], [
            'title' => $way,
        ]);

        if ($products) {
            $productsArray = $this->get('product_service')->processProducts($products);

            return $this->json($productsArray);
        } else {
            return $this->json(['success' => false], 400);
        }
    }

    /**
     * Return all products with discounted prices, sorted by price
     *
     * @Route("/price", name="all_products_ordered_price")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getAllProductsSortedByPriceAction(Request $request)
    {
        $way = $request->get('order');
        if (!$way) {
            $way = 'asc';
        }

        $products = $this->getDoctrine()->getRepository('ApiBundle:Product')->findAll();

        if ($products) {
            $productsArray = $this->get('product_service')->processProducts($products);

            $sortedArray = $this->sortByPrice($productsArray, $way);

            return $this->json($sortedArray);
        } else {
            return $this->json(['success' => false], 400);
        }
    }

    /**
     * Sort array of products by discounted price
     *
     * @param array  $productsArray
     * @param string $way
     *
     * @return array
     */
    private function sortByPrice($productsArray, $way)
    {
        $result = [];
        foreach ($productsArray as $key => $row) {
            $result[$key] = $row['discountedPrice'];
        }

        if ($way === 'asc') {
            array_multisort($result, SORT_ASC, $productsArray);
        } else {
            array_multisort($result, SORT_DESC, $productsArray);
        }

        return $productsArray;

    }
}
