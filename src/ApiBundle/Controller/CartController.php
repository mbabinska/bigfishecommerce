<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\Cart;
use ApiBundle\Entity\CartHasProduct;
use ApiBundle\Entity\Discount;
use ApiBundle\Entity\Product;
use ApiBundle\Entity\Publisher;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class CartController
 *
 * @package ApiBundle\Controller
 * @Route("/api/cart")
 */
class CartController extends Controller
{
    /**
     * Return all products and sum info from user cart
     *
     * @Route("/{user_id}", name="all_cart")
     *
     * @param int $user_id
     *
     * @return JsonResponse
     */
    public function getCartData($user_id)
    {
        if ($user_id) {
            $cart = $this->getDoctrine()->getRepository('ApiBundle:Cart')->findOneBy([
                'userId' => $user_id,
            ]);

            $cartData = $this->processCart($cart);

            $cartArray = [
                'sumAllPrice'        => $cartData['sumAllPrice'],
                'sumDiscountedPrice' => $cartData['sumDiscountedPrice'],
                'discount'           => $cartData['discount'],
                'numberOfproducts'   => $cartData['numberOfproducts'],
                'products'           => $cartData['productsInfoArray'],
            ];

            return $this->json($cartArray);


        }

        return $this->json(['success' => false], 403);
    }

    /**
     * Add product to cart
     *
     * @Route("/add/{user_id}/{product_id}", name="add_product_to_cart")
     *
     * @param int $product_id
     * @param int $user_id
     *
     * @return JsonResponse
     */
    public function addProductToCartAction($product_id, $user_id)
    {
        /** @var Product $product */
        $product = $this->getDoctrine()->getRepository('ApiBundle:Product')->find($product_id);

        if ($user_id) {
            $cart = $this->getDoctrine()->getRepository('ApiBundle:Cart')->findOneBy([
                'userId' => $user_id,
            ]);

            if ($cart) {
                $cartHasProduct = $this->getDoctrine()->getRepository('ApiBundle:CartHasProduct')->findOneBy([
                    'product' => $product_id,
                ]);
                if ($cartHasProduct) {
                    $increasedAmount = $cartHasProduct->getAmount() + 1;
                    $cartHasProduct->setAmount($increasedAmount);
                    $this->getDoctrine()->getManager()->persist($cartHasProduct);
                } else {
                    $cartHasProduct = new CartHasProduct();
                    $cartHasProduct->setProduct($product);
                    $cartHasProduct->setCart($cart);
                    $cartHasProduct->setAmount(1);

                    $this->getDoctrine()->getManager()->persist($cartHasProduct);
                }
            } else {
                $cart = new Cart();
                $cart->setUserId($user_id);

                $cartHasProduct = new CartHasProduct();
                $cartHasProduct->setProduct($product);
                $cartHasProduct->setCart($cart);
                $cartHasProduct->setAmount(1);

                $this->getDoctrine()->getManager()->persist($cart);
                $this->getDoctrine()->getManager()->persist($cartHasProduct);
            }

            $this->getDoctrine()->getManager()->persist($product);

            $this->getDoctrine()->getManager()->flush();

            $cartData = $this->processCart($this->getDoctrine()->getRepository('ApiBundle:Cart')->findOneBy([
                'userId' => $user_id,
            ]));

            $cartArray = [
                'sumAllPrice'        => $cartData['sumAllPrice'],
                'sumDiscountedPrice' => $cartData['sumDiscountedPrice'],
                'discount'           => $cartData['discount'],
                'numberOfproducts'   => $cartData['numberOfproducts'],
                'products'           => $cartData['productsInfoArray'],
            ];

            return $this->json($cartArray);
        }

        return $this->json(['success' => false], 403);
    }

    /**
     * Remove product (amount = 1) from cart
     *
     * @Route("/remove/{user_id}/{product_id}", name="remove_product_from_cart")
     *
     * @param int $product_id
     * @param int $user_id
     *
     * @return JsonResponse
     */
    public function removeProductFromCartAction($product_id, $user_id)
    {
        /** @var Product $product */
        $product = $this->getDoctrine()->getRepository('ApiBundle:Product')->find($product_id);

        if ($user_id) {
            $cart = $this->getDoctrine()->getRepository('ApiBundle:Cart')->findOneBy([
                'userId' => $user_id,
            ]);

            if ($cart) {
                $cartHasProduct = $this->getDoctrine()->getRepository('ApiBundle:CartHasProduct')->findOneBy([
                    'product' => $product_id,
                ]);
                if ($cartHasProduct && $cartHasProduct->getAmount() >= 2) {
                    $decreasedAmount = $cartHasProduct->getAmount() - 1;
                    $cartHasProduct->setAmount($decreasedAmount);
                    $this->getDoctrine()->getManager()->persist($cartHasProduct);
                } elseif ($cartHasProduct && $cartHasProduct->getAmount() === 1) {
                    $this->getDoctrine()->getManager()->remove($cartHasProduct);
                } else {
                    return $this->json(['success' => false], 400);
                }
            } else {
                return $this->json(['success' => false], 400);
            }

            $this->getDoctrine()->getManager()->persist($product);

            $this->getDoctrine()->getManager()->flush();

            $cartData = $this->processCart($cart);

            $cartArray = [
                'sumAllPrice'        => $cartData['sumAllPrice'],
                'sumDiscountedPrice' => $cartData['sumDiscountedPrice'],
                'discount'           => $cartData['discount'],
                'numberOfproducts'   => $cartData['numberOfproducts'],
                'products'           => $cartData['productsInfoArray'],
            ];

            return $this->json($cartArray);
        }

        return $this->json(['success' => false], 403);
    }

    /**
     * Remove all amount of product from cart
     *
     * @Route("/remove/all/{user_id}/{product_id}", name="remove_all_product_from_cart")
     *
     * @param int $product_id
     * @param int $user_id
     *
     * @return JsonResponse
     */
    public function removeAllAmountOfProdactFromCartAction($product_id, $user_id)
    {
        /** @var Product $product */
        $product = $this->getDoctrine()->getRepository('ApiBundle:Product')->find($product_id);

        if ($user_id) {
            $cart = $this->getDoctrine()->getRepository('ApiBundle:Cart')->findOneBy([
                'userId' => $user_id,
            ]);

            if ($cart) {
                $cartHasProduct = $this->getDoctrine()->getRepository('ApiBundle:CartHasProduct')->findOneBy([
                    'product' => $product_id,
                ]);
                if ($cartHasProduct) {
                    $this->getDoctrine()->getManager()->remove($cartHasProduct);
                } else {
                    return $this->json(['success' => false], 400);
                }
            } else {
                return $this->json(['success' => false], 400);
            }

            $this->getDoctrine()->getManager()->persist($product);

            $this->getDoctrine()->getManager()->flush();

            $cartData = $this->processCart($cart);

            $cartArray = [
                'sumAllPrice'        => $cartData['sumAllPrice'],
                'sumDiscountedPrice' => $cartData['sumDiscountedPrice'],
                'discount'           => $cartData['discount'],
                'numberOfproducts'   => $cartData['numberOfproducts'],
                'products'           => $cartData['productsInfoArray'],
            ];

            return $this->json($cartArray);
        }

        return $this->json(['success' => false], 403);
    }

    /**
     * Process cart data: apply and return discount
     *
     * @param Cart $cart
     *
     * @return array
     */
    private function processCart($cart)
    {
        $publisherPackageDiscountProducts = [];
        $productsInfoArray = [];
        $cartInfo = [];

        if ($cart) {
            $cartHasProducts = $cart->getCartHasProducts();

            if ($cartHasProducts) {
                /** @var CartHasProduct $chp */
                foreach ($cartHasProducts as $chp) {
                    $product = $chp->getProduct();
                    $amount = $chp->getAmount();
                    $productInfo = $this->get('product_service')->processProduct($product, $amount);
                    $productsInfoArray[] = $productInfo;

                    $publisher = $product->getPublisher();
                    $publisherHasDiscount = $this->processPublisherPackageDiscounts($publisher, $product->getId(), $amount);
                    if ($publisherHasDiscount) {
                        $publisherPackageDiscountProducts[$publisher->getId()][] = $publisherHasDiscount;
                    }
                }
                $cartInfo = $this->countCartSums($productsInfoArray, $publisherPackageDiscountProducts);
            }
        }

        return $cartInfo;
    }

    /**
     * Return product from cart sorted by publisher, which has package discount
     *
     * @param Publisher $publisher
     * @param int       $productId
     * @param int       $amount
     *
     * @return array|bool
     */
    private function processPublisherPackageDiscounts($publisher, $productId, $amount)
    {
        $publisherDiscounts = $publisher->getDiscount();

        if ($publisherDiscounts) {
            /** @var Discount $discount */
            foreach ($publisherDiscounts as $discount) {
                $discountType = $discount->getType()->getName();
                if ($discountType === 'package') {
                    $product = $this->getDoctrine()->getRepository('ApiBundle:Product')->find($productId);

                    return $this->get('product_service')->processProduct($product, $amount);
                }
            }
        } else {
            return false;
        }
    }

    /**
     * @param array $productsInfoArray
     * @param array $publisherPackageDiscountProducts
     *
     * @return array
     */
    private function countCartSums($productsInfoArray, $publisherPackageDiscountProducts)
    {
        $publisherCountedData = $this->countCartSumsBasedOnPublisherPackageDiscount($productsInfoArray, $publisherPackageDiscountProducts);

        $processedProductsIds = $publisherCountedData['processedProductsIds'];
        $productsInfoArray = $publisherCountedData['productsInfoArray'];

        $sumAllPrice = 0;
        $sumDiscountedPrice = 0;

        foreach ($productsInfoArray as $product) {
            $isProcessed = in_array($product['id'], $processedProductsIds);
            if (!$isProcessed) {
                $sumAllPrice += $product['amount'] * $product['price'];
                $sumDiscountedPrice += $product['amountAfterPackageDiscount'] * $product['discountedPrice'];
                $processedProductsIds[] = $product['id'];
            } else {
                $sumAllPrice += $product['amount'] * $product['discountedPrice'];
            }
        }

        return [
            'sumAllPrice'        => $sumAllPrice,
            'discount'           => $sumAllPrice - $sumDiscountedPrice,
            'sumDiscountedPrice' => $sumDiscountedPrice,
            'numberOfproducts'   => $this->sumOfAllProducts($productsInfoArray),
            'productsInfoArray'  => $productsInfoArray,
        ];
    }

    private function countCartSumsBasedOnPublisherPackageDiscount($productsInfoArray, $publisherPackageDiscountProducts)
    {
        $processedProductsIds = [];

        /**
         * @var  Publisher $key
         * @var  array     $value
         */
        foreach ($publisherPackageDiscountProducts as $key => $value) {
            /**
             * Discount which will be apply can be choosed by programmer. The first discount is choosed now
             */
            $publisher = $this->getDoctrine()->getRepository('ApiBundle:Publisher')->find($key);
            $publisherDiscounts = $publisher->getDiscount();
            /** @var Discount $publisherChoosedDiscount */
            $publisherChoosedDiscount = $publisherDiscounts[0];
            $publisherName = $publisher->getName();

            $payPackageRule = $publisherChoosedDiscount->getPayPackage();
            $freePackageRule = $publisherChoosedDiscount->getFreePackage();
            $packageAmountForDiscount = $payPackageRule + $freePackageRule;
            $sumOfPublisherProductsInCart = $this->sumOfPublisherProducts($productsInfoArray, $publisherName);

            /** Apply package discount: save Id to processed Id or decrease amount for pay */
            if ($packageAmountForDiscount <= $sumOfPublisherProductsInCart) {
                $numberOfApplyDiscount = floor($sumOfPublisherProductsInCart / $packageAmountForDiscount);

                $publisherPackageDiscountProductsInner = $publisherPackageDiscountProducts[$publisher->getId()];
                usort($publisherPackageDiscountProductsInner, function ($a, $b) {
                    return $a['discountedPrice'] - $b['discountedPrice'];
                });

                $possitionOfProductWithLowestPrice = 0;

                for ($number3 = 0; $number3 < $numberOfApplyDiscount; $number3++) {
                    /** Number of packages, which should be free in current discount */
                    for ($number = 0; $number < $freePackageRule; $number++) {

                        if ($publisherPackageDiscountProductsInner[$possitionOfProductWithLowestPrice]['amount'] === 1) {
                            $processedProductsIds[] = $publisherPackageDiscountProductsInner[$possitionOfProductWithLowestPrice]['id'];

                            $publisherPackageDiscountProductsInner[$possitionOfProductWithLowestPrice]['amount'] -= 1;
                            $number4 = 0;
                            foreach ($productsInfoArray as $product) {
                                if ($product['id'] === $publisherPackageDiscountProductsInner[$possitionOfProductWithLowestPrice]['id']) {
                                    $productsInfoArray[$number4]['amountAfterPackageDiscount'] = 0;
                                }
                                $number4++;
                            }
                            $possitionOfProductWithLowestPrice++;

                        } else {
                            $number2 = 0;

                            foreach ($productsInfoArray as $product) {
                                if ($product['id'] === $publisherPackageDiscountProductsInner[$possitionOfProductWithLowestPrice]['id']) {
                                    $publisherPackageDiscountProductsInner[$possitionOfProductWithLowestPrice]['amount'] -= 1;
                                    $productsInfoArray[$number2]['amountAfterPackageDiscount'] = $publisherPackageDiscountProductsInner[$possitionOfProductWithLowestPrice]['amount'];
                                }
                                $number2++;
                            }
                        }
                        $number++;
                    }
                }
            }
        }

        return [
            'processedProductsIds' => $processedProductsIds,
            'productsInfoArray'    => $productsInfoArray,
        ];
    }

    /**
     * @param array $productsInfoArray
     *
     * @return int
     */
    private function sumOfAllProducts($productsInfoArray)
    {
        $sumOfProducts = 0;

        foreach ($productsInfoArray as $piA) {
            $sumOfProducts += $piA['amount'];
        }

        return $sumOfProducts;
    }

    /**
     * @param array  $productsInfoArray
     * @param string $publisher
     *
     * @return int
     */
    private function sumOfPublisherProducts($productsInfoArray, $publisher)
    {
        $sumOfProducts = 0;

        foreach ($productsInfoArray as $piA) {
            if ($piA['publisher'] === $publisher) {
                $sumOfProducts += $piA['amount'];
            }
        }

        return $sumOfProducts;
    }
}
