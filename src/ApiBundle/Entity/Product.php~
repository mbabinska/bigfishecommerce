<?php

namespace ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Product
 *
 * @ORM\Table(name="product")
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\ProductRepository")
 */
class Product
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", nullable=false)
     * @Assert\NotNull()
     */
    private $price;

    /**
     * @var Publisher
     *
     * @ORM\ManyToOne(targetEntity="Publisher", inversedBy="product")
     * @ORM\JoinColumn(name="publisher_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank()
     */
    private $publisher;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Author", inversedBy="products")
     * @ORM\JoinTable(name="product_has_author")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id")
     */
    private $authors;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Discount", inversedBy="products")
     * @ORM\JoinTable(name="product_has_discount")
     * @ORM\JoinColumn(name="discount_id", referencedColumnName="id")
     */
    private $discounts;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="CartHasProduct", mappedBy="product")
     */
    private $cartHasProducts;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->authors = new ArrayCollection();
        $this->discounts = new ArrayCollection();
        $this->cartHasProducts = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Product
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set publisher
     *
     * @param Publisher $publisher
     *
     * @return Product
     */
    public function setPublisher(Publisher $publisher = null)
    {
        $this->publisher = $publisher;

        return $this;
    }

    /**
     * Get publisher
     *
     * @return Publisher
     */
    public function getPublisher()
    {
        return $this->publisher;
    }

    /**
     * Add author
     *
     * @param Author $author
     *
     * @return Product
     */
    public function addAuthor(Author $author)
    {
        $author->addProduct($this);
        $this->authors[] = $author;

        return $this;
    }

    /**
     * Remove author
     *
     * @param Author $author
     */
    public function removeAuthor(Author $author)
    {
        $this->authors->removeElement($author);
    }

    /**
     * Get authors
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAuthors()
    {
        return $this->authors;
    }

    /**
     * Add discount
     *
     * @param Discount $discount
     *
     * @return Product
     */
    public function addDiscount(Discount $discount)
    {
        $this->discounts[] = $discount;

        return $this;
    }

    /**
     * Remove discount
     *
     * @param Discount $discount
     */
    public function removeDiscount(Discount $discount)
    {
        $this->discounts->removeElement($discount);
    }

    /**
     * Get discounts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDiscounts()
    {
        return $this->discounts;
    }

    /**
     * Add cartHasProduct
     *
     * @param CartHasProduct $cartHasProduct
     *
     * @return Product
     */
    public function addCartHasProduct(CartHasProduct $cartHasProduct)
    {
        $this->cartHasProducts[] = $cartHasProduct;

        return $this;
    }

    /**
     * Remove cartHasProduct
     *
     * @param CartHasProduct $cartHasProduct
     */
    public function removeCartHasProduct(CartHasProduct $cartHasProduct)
    {
        $this->cartHasProducts->removeElement($cartHasProduct);
    }

    /**
     * Get cartHasProducts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCartHasProducts()
    {
        return $this->cartHasProducts;
    }
}
