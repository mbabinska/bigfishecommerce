<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CartHasProduct
 *
 * @ORM\Table(name="cart_has_product")
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\CartHasProductRepository")
 */
class CartHasProduct
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="amount", type="integer", nullable=false, options={"default":1})
     */
    private $amount;

    /**
     * @var Cart
     *
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\Cart", inversedBy="cartHasProducts")
     * @ORM\JoinColumn(name="cart_id", referencedColumnName="id")
     */
    private $cart;

    /**
     * @var Product
     *
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\Product", inversedBy="cartHasProducts")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $product;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     *
     * @return CartHasProduct
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set cart
     *
     * @param Cart $cart
     *
     * @return CartHasProduct
     */
    public function setCart(Cart $cart = null)
    {
        $cart->addCartHasProduct($this);
        $this->cart = $cart;

        return $this;
    }

    /**
     * Get cart
     *
     * @return Cart
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * Set product
     *
     * @param Product $product
     *
     * @return CartHasProduct
     */
    public function setProduct(Product $product = null)
    {
        $product->addCartHasProduct($this);
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \ApiBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }
}
