<?php

namespace ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Publisher
 *
 * @ORM\Table(name="publisher")
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\PublisherRepository")
 */
class Publisher
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Product", mappedBy="publisher")
     */
    private $product;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Discount", inversedBy="publishers")
     * @ORM\JoinTable(name="publisher_has_discount")
     * @ORM\JoinColumn(name="discount_id", referencedColumnName="id")
     */
    private $discount;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->product = new ArrayCollection();
        $this->discount = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Publisher
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add product
     *
     * @param Product $product
     *
     * @return Publisher
     */
    public function addProduct(Product $product)
    {
        $this->product[] = $product;

        return $this;
    }

    /**
     * Remove product
     *
     * @param Product $product
     */
    public function removeProduct(Product $product)
    {
        $this->product->removeElement($product);
    }

    /**
     * Get product
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Add discount
     *
     * @param Discount $discount
     *
     * @return Publisher
     */
    public function addDiscount(Discount $discount)
    {
        $this->discount[] = $discount;

        return $this;
    }

    /**
     * Remove discount
     *
     * @param Discount $discount
     */
    public function removeDiscount(Discount $discount)
    {
        $this->discount->removeElement($discount);
    }
    
    /**
     * Get discount
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDiscount()
    {
        return $this->discount;
    }
}
