<?php

namespace ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Discount
 *
 * @ORM\Table(name="discount")
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\DiscountRepository")
 */
class Discount
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text", nullable=false)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float", nullable=true)
     */
    private $amount;

    /**
     * @var bool
     *
     * @ORM\Column(name="percentage", type="boolean", options={"default" : 0}, nullable=true)
     */
    private $percentage;

    /**
     * @var bool
     *
     * @ORM\Column(name="all_products", type="boolean", options={"default" : 0}, nullable=true)
     */
    private $allProducts;

    /**
     * @var integer
     *
     * @ORM\Column(name="pay_package", type="integer", nullable=true)
     */
    private $payPackage;

    /**
     * @var integer
     *
     * @ORM\Column(name="free_package", type="integer", nullable=true)
     */
    private $freePackage;

    /**
     * @var DiscountType
     *
     * @ORM\ManyToOne(targetEntity="DiscountType", inversedBy="dicsount")
     * @ORM\JoinColumn(name="discount_type_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $type;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Product", mappedBy="discount")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $products;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Publisher", mappedBy="discount")
     * @ORM\JoinColumn(name="publisher_id", referencedColumnName="id")
     */
    private $publishers;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->publishers = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Discount
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return Discount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set percentage
     *
     * @param boolean $percentage
     *
     * @return Discount
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;

        return $this;
    }

    /**
     * Get percentage
     *
     * @return bool
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * Set allProducts
     *
     * @param boolean $allProducts
     *
     * @return Discount
     */
    public function setAllProducts($allProducts)
    {
        $this->allProducts = $allProducts;

        return $this;
    }

    /**
     * Get allProducts
     *
     * @return bool
     */
    public function getAllProducts()
    {
        return $this->allProducts;
    }

    /**
     * Set payPackage
     *
     * @param integer $payPackage
     *
     * @return Discount
     */
    public function setPayPackage($payPackage)
    {
        $this->payPackage = $payPackage;

        return $this;
    }

    /**
     * Get payPackage
     *
     * @return integer
     */
    public function getPayPackage()
    {
        return $this->payPackage;
    }

    /**
     * Set freePackage
     *
     * @param integer $freePackage
     *
     * @return Discount
     */
    public function setFreePackage($freePackage)
    {
        $this->freePackage = $freePackage;

        return $this;
    }

    /**
     * Get freePackage
     *
     * @return integer
     */
    public function getFreePackage()
    {
        return $this->freePackage;
    }

    /**
     * Set type
     *
     * @param DiscountType $type
     *
     * @return Discount
     */
    public function setType(DiscountType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return DiscountType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add product
     *
     * @param Product $product
     *
     * @return Discount
     */
    public function addProduct(Product $product)
    {
        $product->addDiscount($this);
        $this->products[] = $product;

        return $this;
    }

    /**
     * Remove product
     *
     * @param Product $product
     */
    public function removeProduct(Product $product)
    {
        $this->products->removeElement($product);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Add publisher
     *
     * @param Publisher $publisher
     *
     * @return Discount
     */
    public function addPublisher(Publisher $publisher)
    {
        $publisher->addDiscount($this);
        $this->publishers[] = $publisher;

        return $this;
    }

    /**
     * Remove publisher
     *
     * @param Publisher $publisher
     */
    public function removePublisher(Publisher $publisher)
    {
        $this->publishers->removeElement($publisher);
    }

    /**
     * Get publishers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPublishers()
    {
        return $this->publishers;
    }
}
