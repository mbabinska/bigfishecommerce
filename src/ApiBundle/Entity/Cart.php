<?php

namespace ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Cart
 *
 * @ORM\Table(name="cart")
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\CartRepository")
 */
class Cart
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer", unique=true)
     */
    private $userId;

    /**
     * @var ArrayCollection
     * 
     * @ORM\OneToMany(targetEntity="CartHasProduct", mappedBy="cart")
     */
    private $cartHasProducts;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cartHasProducts = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Cart
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Add cartHasProduct
     *
     * @param CartHasProduct $cartHasProduct
     *
     * @return Cart
     */
    public function addCartHasProduct(CartHasProduct $cartHasProduct)
    {
        $this->cartHasProducts[] = $cartHasProduct;

        return $this;
    }

    /**
     * Remove cartHasProduct
     *
     * @param CartHasProduct $cartHasProduct
     */
    public function removeCartHasProduct(CartHasProduct $cartHasProduct)
    {
        $this->cartHasProducts->removeElement($cartHasProduct);
    }

    /**
     * Get cartHasProducts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCartHasProducts()
    {
        return $this->cartHasProducts;
    }
}
