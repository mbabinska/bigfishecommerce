-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Hostiteľ: 127.0.0.1
-- Čas generovania: Ne 24.Júl 2016, 19:38
-- Verzia serveru: 10.1.13-MariaDB
-- Verzia PHP: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáza: `bigfishecommerce`
--
CREATE DATABASE IF NOT EXISTS `bigfishecommerce` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `bigfishecommerce`;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `author`
--

CREATE TABLE `author` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Sťahujem dáta pre tabuľku `author`
--

INSERT INTO `author` (`id`, `name`) VALUES
(81, 'Janine Warner'),
(82, 'Sikos László'),
(83, 'Barry Burd'),
(84, 'Stephen Randy Davis'),
(85, 'Joshua Eichorn'),
(86, 'Ivanyos Gábor'),
(87, 'Rónyai Lajos'),
(88, 'Szabó Réka');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `cart_has_product`
--

CREATE TABLE `cart_has_product` (
  `id` int(11) NOT NULL,
  `cart_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `amount` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `discount`
--

CREATE TABLE `discount` (
  `id` int(11) NOT NULL,
  `name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `amount` double DEFAULT NULL,
  `percentage` tinyint(1) DEFAULT '0',
  `all_products` tinyint(1) DEFAULT '0',
  `discount_type_id` int(11) DEFAULT NULL,
  `pay_package` int(11) DEFAULT NULL,
  `free_package` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Sťahujem dáta pre tabuľku `discount`
--

INSERT INTO `discount` (`id`, `name`, `amount`, `percentage`, `all_products`, `discount_type_id`, `pay_package`, `free_package`) VALUES
(2, 'discount percent of 10%, calculated from item price', 10, 1, NULL, 11, NULL, NULL),
(3, 'amount discount of 500 HUF, calculated from item price', 500, 0, NULL, 11, NULL, NULL),
(4, '2+1 package discount (the cheapest product in a set is free)', NULL, NULL, NULL, 12, 2, 1);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `discount_type`
--

CREATE TABLE `discount_type` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Sťahujem dáta pre tabuľku `discount_type`
--

INSERT INTO `discount_type` (`id`, `name`) VALUES
(11, 'amount'),
(12, 'package');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `publisher_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Sťahujem dáta pre tabuľku `product`
--

INSERT INTO `product` (`id`, `publisher_id`, `title`, `price`) VALUES
(32, 28, 'Dreamweaver CS4', 3900),
(33, 29, 'JavaScript on client side', 2900),
(34, 28, 'Java', 3700),
(35, 28, 'C# 2008', 3700),
(36, 28, 'Ajax Fundamentals', 4500),
(37, 30, 'Algorithms', 3600);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `product_has_author`
--

CREATE TABLE `product_has_author` (
  `product_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Sťahujem dáta pre tabuľku `product_has_author`
--

INSERT INTO `product_has_author` (`product_id`, `author_id`) VALUES
(32, 81),
(33, 82),
(34, 83),
(35, 84),
(36, 85),
(37, 86),
(37, 87),
(37, 88);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `product_has_discount`
--

CREATE TABLE `product_has_discount` (
  `product_id` int(11) NOT NULL,
  `discount_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Sťahujem dáta pre tabuľku `product_has_discount`
--

INSERT INTO `product_has_discount` (`product_id`, `discount_id`) VALUES
(33, 3),
(37, 2);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `publisher`
--

CREATE TABLE `publisher` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Sťahujem dáta pre tabuľku `publisher`
--

INSERT INTO `publisher` (`id`, `name`) VALUES
(28, 'PANEM'),
(29, 'BBS-INFO'),
(30, 'TYPOTEX');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `publisher_has_discount`
--

CREATE TABLE `publisher_has_discount` (
  `publisher_id` int(11) NOT NULL,
  `discount_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Sťahujem dáta pre tabuľku `publisher_has_discount`
--

INSERT INTO `publisher_has_discount` (`publisher_id`, `discount_id`) VALUES
(28, 4);

--
-- Kľúče pre exportované tabuľky
--

--
-- Indexy pre tabuľku `author`
--
ALTER TABLE `author`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pre tabuľku `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_BA388B7A76ED395` (`user_id`);

--
-- Indexy pre tabuľku `cart_has_product`
--
ALTER TABLE `cart_has_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_E76895361AD5CDBF` (`cart_id`),
  ADD KEY `IDX_E76895364584665A` (`product_id`);

--
-- Indexy pre tabuľku `discount`
--
ALTER TABLE `discount`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_E1E0B40E7344E182` (`discount_type_id`);

--
-- Indexy pre tabuľku `discount_type`
--
ALTER TABLE `discount_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pre tabuľku `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_D34A04AD40C86FCE` (`publisher_id`);

--
-- Indexy pre tabuľku `product_has_author`
--
ALTER TABLE `product_has_author`
  ADD PRIMARY KEY (`product_id`,`author_id`),
  ADD KEY `IDX_2EA90784584665A` (`product_id`),
  ADD KEY `IDX_2EA9078F675F31B` (`author_id`);

--
-- Indexy pre tabuľku `product_has_discount`
--
ALTER TABLE `product_has_discount`
  ADD PRIMARY KEY (`product_id`,`discount_id`),
  ADD KEY `IDX_7D226C514584665A` (`product_id`),
  ADD KEY `IDX_7D226C514C7C611F` (`discount_id`);

--
-- Indexy pre tabuľku `publisher`
--
ALTER TABLE `publisher`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pre tabuľku `publisher_has_discount`
--
ALTER TABLE `publisher_has_discount`
  ADD PRIMARY KEY (`publisher_id`,`discount_id`),
  ADD KEY `IDX_35CB0A9C40C86FCE` (`publisher_id`),
  ADD KEY `IDX_35CB0A9C4C7C611F` (`discount_id`);

--
-- AUTO_INCREMENT pre exportované tabuľky
--

--
-- AUTO_INCREMENT pre tabuľku `author`
--
ALTER TABLE `author`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT pre tabuľku `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT pre tabuľku `cart_has_product`
--
ALTER TABLE `cart_has_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT pre tabuľku `discount`
--
ALTER TABLE `discount`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pre tabuľku `discount_type`
--
ALTER TABLE `discount_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pre tabuľku `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT pre tabuľku `publisher`
--
ALTER TABLE `publisher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- Obmedzenie pre exportované tabuľky
--

--
-- Obmedzenie pre tabuľku `cart_has_product`
--
ALTER TABLE `cart_has_product`
  ADD CONSTRAINT `FK_E76895361AD5CDBF` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`id`),
  ADD CONSTRAINT `FK_E76895364584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`);

--
-- Obmedzenie pre tabuľku `discount`
--
ALTER TABLE `discount`
  ADD CONSTRAINT `FK_E1E0B40E7344E182` FOREIGN KEY (`discount_type_id`) REFERENCES `discount_type` (`id`);

--
-- Obmedzenie pre tabuľku `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `FK_D34A04AD40C86FCE` FOREIGN KEY (`publisher_id`) REFERENCES `publisher` (`id`);

--
-- Obmedzenie pre tabuľku `product_has_author`
--
ALTER TABLE `product_has_author`
  ADD CONSTRAINT `FK_2EA90784584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_2EA9078F675F31B` FOREIGN KEY (`author_id`) REFERENCES `author` (`id`) ON DELETE CASCADE;

--
-- Obmedzenie pre tabuľku `product_has_discount`
--
ALTER TABLE `product_has_discount`
  ADD CONSTRAINT `FK_7D226C514584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_7D226C514C7C611F` FOREIGN KEY (`discount_id`) REFERENCES `discount` (`id`) ON DELETE CASCADE;

--
-- Obmedzenie pre tabuľku `publisher_has_discount`
--
ALTER TABLE `publisher_has_discount`
  ADD CONSTRAINT `FK_35CB0A9C40C86FCE` FOREIGN KEY (`publisher_id`) REFERENCES `publisher` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_35CB0A9C4C7C611F` FOREIGN KEY (`discount_id`) REFERENCES `discount` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
