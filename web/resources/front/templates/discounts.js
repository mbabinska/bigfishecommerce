$(document).ready(function () {
    var source = $("#discounts").html();
    var template = Handlebars.compile(source);

    $.ajax({
        url: '/api/discounts',
        success: function (response) {
            var context = {
                discounts: response
            };
            var html = template(context);

            $('.carousel-inner').html(html);
        }
    });
})
