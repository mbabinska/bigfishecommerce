var product_order_title = 'asc';
var product_order_price = 'asc';

$(document).ready(function () {
    var source = $("#products").html();
    var template = Handlebars.compile(source);

    $.ajax({
        url: '/api/products',
        success: function (response) {
            var context = {
                products: response
            };
            var html = template(context);

            $('#product-list').html(html);
        }
    });
});

$("#order-by-title").click(function () {
    var source = $("#products").html();
    var template = Handlebars.compile(source);
    $('#product-list').html('');

    $.ajax({
        url: '/api/products/title',
        method: 'POST',
        data: {
            order: product_order_title
        },
        success: function (response) {
            var context = {
                products: response
            };
            var html = template(context);

            $('#product-list').html(html);

            if (product_order_title == 'desc') {
                product_order_title = 'asc';
            } else {
                product_order_title = 'desc';
            }
        }
    });
    return false;
});

$("#order-by-price").click(function () {
    var source = $("#products").html();
    var template = Handlebars.compile(source);
    $('#product-list').html('');

    $.ajax({
        url: '/api/products/price',
        method: 'POST',
        data: {
            order: product_order_price
        },
        success: function (response) {
            var context = {
                products: response
            };
            var html = template(context);

            $('#product-list').html(html);

            if (product_order_price == 'desc') {
                product_order_price = 'asc';
            } else {
                product_order_price = 'desc';
            }
        }
    });
    return false;
});

$(document).ready(function () {
    var source = $("#cart-icon").html();
    var template = Handlebars.compile(source);

    $.ajax({
        url: '/api/cart/10',
        success: function (response) {
            var html = template(response);

            $('#cart-icon-handler').html(html);
        }
    });
});

$(document).on('click', '.add-product-to-cart', function () {
    var pid = $(this).attr("data-product");
    var source = $("#cart-icon").html();
    var template = Handlebars.compile(source);

    $.ajax({
        url: '/api/cart/add/10/' + pid,
        method: 'POST',
        success: function (response) {
            var html = template(response);

            $('#cart-icon-handler').html(html);
        }
    });
    return false;
});

