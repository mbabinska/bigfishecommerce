$(document).ready(function () {
    var source = $("#cart-icon").html();
    var template = Handlebars.compile(source);

    $.ajax({
        url: '/api/cart/10',
        success: function (response) {
            var html = template(response);

            $('#cart-icon-handler').html(html);
        }
    });
});

$(document).ready(function () {
    var source = $("#cart").html();
    var template = Handlebars.compile(source);

    $.ajax({
        url: '/api/cart/10',
        success: function (response) {
            var html = template(response);

            $('#cart_product_list').html(html);
        }
    });
});

$(document).on('click', '.cart_quantity_up', function () {
    var pid = $(this).attr("data-product");
    var source = $("#cart").html();
    var template = Handlebars.compile(source);

    $.ajax({
        url: '/api/cart/add/10/' + pid,
        method: 'POST',
        success: function (response) {
            var html = template(response);

            $('#cart_product_list').html(html);
        }
    });
    return false;
});

$(document).on('click', '.cart_quantity_up', function () {
    var source = $("#cart-icon").html();
    var template = Handlebars.compile(source);

    $.ajax({
        url: '/api/cart/10',
        success: function (response) {
            var html = template(response);

            $('#cart-icon-handler').html(html);
        }
    });
});

$(document).on('click', '.cart_quantity_down', function () {
    var pid = $(this).attr("data-product");
    var source = $("#cart").html();
    var template = Handlebars.compile(source);

    $.ajax({
        url: '/api/cart/remove/10/' + pid,
        method: 'POST',
        success: function (response) {
            var html = template(response);

            $('#cart_product_list').html(html);
        }
    });
    return false;
});

$(document).on('click', '.cart_quantity_down', function () {
    var source = $("#cart-icon").html();
    var template = Handlebars.compile(source);

    $.ajax({
        url: '/api/cart/10',
        success: function (response) {
            var html = template(response);

            $('#cart-icon-handler').html(html);
        }
    });
});

$(document).on('click', '.cart_quantity_delete', function () {
    var pid = $(this).attr("data-product");
    var source = $("#cart").html();
    var template = Handlebars.compile(source);

    $.ajax({
        url: '/api/cart/remove/all/10/' + pid,
        method: 'POST',
        success: function (response) {
            var html = template(response);

            $('#cart_product_list').html(html);
        }
    });
    return false;
});

$(document).on('click', '.cart_quantity_delete', function () {
    var source = $("#cart-icon").html();
    var template = Handlebars.compile(source);

    $.ajax({
        url: '/api/cart/10',
        success: function (response) {
            var html = template(response);

            $('#cart-icon-handler').html(html);
        }
    });
});
